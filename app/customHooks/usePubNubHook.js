import React, {useEffect} from 'react';
import {usePubNub} from 'pubnub-react';
import {useDispatch, useSelector} from 'react-redux';

import {disableSignal, enableSignal} from '../redux/actions';
import {dialogSelector} from './useDialogSelector';

export const UsePubNubHook = () => {
  const {clientID, channels} = useSelector(dialogSelector);
  const pubnub = usePubNub();
  const dispatch = useDispatch();

  const handleSignal = event => {
    const message = event.message;
    const publisher = event.publisher;
    message === 'typing_on' && publisher !== clientID
      ? dispatch(enableSignal())
      : dispatch(disableSignal());
  };

  const handleEnableSignal = () => {
    pubnub.signal({message: 'typing_on', channel: channels[0]}, () => {});
  };

  const handleDisableSignal = () => {
    pubnub.signal({message: 'typing_off', channel: channels[0]}, () => {});
  };

  useEffect(() => {
    if (channels) {
      pubnub.addListener({signal: handleSignal});
      pubnub.subscribe({channels});
    }
    return () => {
      pubnub.removeAllListeners();
      pubnub.unsubscribeAll();
    };
  }, [pubnub, channels]);

  return {handleEnableSignal, handleDisableSignal};
};
