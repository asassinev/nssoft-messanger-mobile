import {
  CHANGE_STATUS,
  SAVE_DIALOG,
  SAVE_CLIENT_ID,
  END_DIALOG,
  ENABLE_SIGNAL,
  DISABLE_SIGNAL,
} from './types';

const initialState = {
  clientID: null,
  dialog: null,
  status: 'home',
  channels: [],
  typing: false,
};

const dialogReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_CLIENT_ID:
      return {
        ...state,
        clientID: action.payload,
      };
    case SAVE_DIALOG:
      return {
        ...state,
        dialog: action.payload,
        channels: [action.payload.id],
      };
    case CHANGE_STATUS:
      return {
        ...state,
        status: action.payload,
      };
    case END_DIALOG:
      return {
        status: 'home',
        dialog: null,
        channels: [],
      };
    case ENABLE_SIGNAL:
      return {
        ...state,
        typing: true,
      };
    case DISABLE_SIGNAL:
      return {
        ...state,
        typing: false,
      };
    default:
      return state;
  }
};

export default dialogReducer;
