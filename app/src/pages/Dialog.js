import React from 'react';
import {useSelector} from 'react-redux';
import PubNub from 'pubnub';
import {PubNubProvider} from 'pubnub-react';
import {PUBLISH_KEY_PUBNUB, SUBSCRIBE_KEY} from '@env';

import Chat from '../layout/Chat';
import {dialogSelector} from '../../customHooks/useDialogSelector';

export const Dialog = () => {
  const {clientID} = useSelector(dialogSelector);
  const pubNub = new PubNub({
    publishKey: PUBLISH_KEY_PUBNUB,
    subscribeKey: SUBSCRIBE_KEY,
    uuid: clientID,
    subscribeRequestTimeout: 60000,
    presenceTimeout: 122,
  });

  return (
    <PubNubProvider client={pubNub}>
      <Chat />
    </PubNubProvider>
  );
};
