import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {firebase} from '@react-native-firebase/database';
import {View, Text, Button, StyleSheet} from 'react-native';

import {changeStatus} from '../../redux/actions';
import {addNotification} from '../utils/firebaseRequests';
import {dialogSelector} from '../../customHooks/useDialogSelector';

export const Queue = () => {
  const [queue, setQueue] = useState(null);
  const {clientID} = useSelector(dialogSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    const queueRef = firebase.database().ref('Waiting');
    queueRef.on('value', snapshot => {
      if (snapshot.val() === null) {
        return;
      }
      const arraysOfKeys = Object.keys(snapshot.val()).reverse();
      let counter = 0;
      arraysOfKeys.map(id => {
        counter++;
        if (snapshot.val()[id].clientID === clientID) {
          setQueue(counter);
        }
      });
    });

    const dialogRef = firebase.database().ref('Active');
    dialogRef.on('value', snapshot => {
      if (snapshot.val() === null) {
        return;
      }
      const arrayOfKeys = Object.keys(snapshot.val());
      let results = null;
      arrayOfKeys.map(id => {
        if (snapshot.val()[id].clientID === clientID) {
          results = snapshot.val()[id];
        }
      });
      if (results) {
        queueRef.off();
        dialogRef.off();
        dispatch(changeStatus('activeDialog'));
        Actions.dialog();
      }
    });
    return () => {
      queueRef.off();
      dialogRef.off();
    };
  }, [clientID, dispatch]);

  const RemindMe = async () => {
    await addNotification(clientID);
  };

  return (
    <View style={styles.queueContainer}>
      <Text style={styles.queueCounter}>Ваше место в очереди: {queue}</Text>
      <Button onPress={RemindMe} title='Напомнить когда придет очередь' />
    </View>
  );
};

const styles = StyleSheet.create({
  queueContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  queueCounter: {
    marginBottom: 20,
  },
});
