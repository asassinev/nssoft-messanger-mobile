import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {Message} from '../message/Message';
import {changeStatus} from '../../../../redux/actions';
import {dialogSelector} from '../../../../customHooks/useDialogSelector';
import {UsePubNubHook} from '../../../../customHooks/usePubNubHook';

export const Messages = () => {
  const {dialog} = useSelector(dialogSelector);
  const {handleDisableSignal} = UsePubNubHook();
  const dispatch = useDispatch();
  const closeDialog = () => {
    handleDisableSignal();
    dispatch(changeStatus('rate'));
    Actions.rate();
  };

  return (
    <>
      {dialog && (
        <>
          <View style={styles.header}>
            <Text style={styles.title}>
              Вы общаетесь с оператором {dialog.operatorName}
            </Text>
            <TouchableOpacity onPress={closeDialog} style={styles.btn}>
              <Text style={styles.btnColor}>Завершить диалог</Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={styles.messages}>
            <Text style={styles.messageCreatedAt}>
              Диалог начат {moment(dialog.startedAt).format('LT')}
            </Text>
            {dialog.messages &&
              dialog.messages.map((message, index) => (
                <Message
                  key={index}
                  message={message}
                  operatorName={dialog.operatorName}
                />
              ))}
          </ScrollView>
        </>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#007aff',
    display: 'flex',
    flexDirection: 'row',
    padding: 20,
  },
  title: {
    fontSize: 20,
    color: '#fafefd',
    width: '70%',
  },
  messageCreatedAt: {
    textAlign: 'center',
    marginBottom: 8,
    marginTop: 8,
  },
  messages: {
    flex: 1,
    backgroundColor: '#f8f9fb',
    paddingHorizontal: 14,
  },
  btn: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fafefd',
    width: '30%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
  },
  btnColor: {
    color: '#fafefd',
  },
});
