import {
  CHANGE_STATUS,
  SAVE_DIALOG,
  SAVE_CLIENT_ID,
  END_DIALOG,
  ENABLE_SIGNAL,
  DISABLE_SIGNAL,
} from './types';

export const saveClientID = payload => {
  return {type: SAVE_CLIENT_ID, payload};
};

export const saveDialog = payload => {
  return {type: SAVE_DIALOG, payload};
};

export const changeStatus = payload => {
  return {
    type: CHANGE_STATUS,
    payload,
  };
};

export const endDialog = () => {
  return {type: END_DIALOG};
};

export const enableSignal = () => {
  return {type: ENABLE_SIGNAL};
};

export const disableSignal = () => {
  return {type: DISABLE_SIGNAL};
};
