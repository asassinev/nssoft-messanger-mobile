import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {firebase} from '@react-native-firebase/database';
import axios from 'axios';
import {
  ActivityIndicator,
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPaperPlane, faPhotoVideo} from '@fortawesome/free-solid-svg-icons';
import {IMGUR_CLIENT_ID} from '@env';

import {dialogSelector} from '../../../../customHooks/useDialogSelector';
import {UsePubNubHook} from '../../../../customHooks/usePubNubHook';

export const MessageInput = ({images, onSendMessage, onUpdateVisible}) => {
  const {dialog} = useSelector(dialogSelector);
  const [loading, setLoading] = useState(false);
  const [messageValue, setMessageValue] = useState('');
  const {handleDisableSignal, handleEnableSignal} = UsePubNubHook();

  const handleInput = value => {
    setMessageValue(value);
    if (value === '') {
      handleDisableSignal();
    } else {
      handleEnableSignal();
    }
  };

  const handleSubmitMessage = async () => {
    setLoading(true);
    handleDisableSignal();
    const dRef = firebase.database().ref('Active/' + dialog.id);
    if (messageValue !== '' || images[0]) {
      let data = new FormData();
      data.append('image', images[0]);
      if (images[0]) {
        axios({
          method: 'POST',
          url: 'https://api.imgur.com/3/image',
          headers: {Authorization: IMGUR_CLIENT_ID},
          data,
        })
          .then(res => {
            let imagesLink = [];

            imagesLink.push(res.data.data.link);

            dialog.messages.push({
              content: messageValue,
              images: imagesLink,
              timestamp: new Date().getTime(),
              writtenBy: 'client',
            });

            setMessageValue('');
            dRef.update({messages: dialog.messages}).then(r => {});
            dRef.off();
            setLoading(false);
            return onSendMessage();
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        dialog.messages.push({
          content: messageValue,
          timestamp: new Date().getTime(),
          writtenBy: 'client',
        });
        dRef.update({messages: dialog.messages}).then(r => {});
        dRef.off();
        setMessageValue('');
        setLoading(false);
      }
    } else {
      setLoading(false);
      Alert.alert('Пустое сообщение!');
      return;
    }
  };

  return (
    <View style={styles.inputContainer}>
      {loading && (
        <ActivityIndicator style={styles.loader} size='large' color='#00ff00' />
      )}
      <ScrollView horizontal={true} style={styles.images}>
        {images &&
          images.map((image, index) => (
            <Image
              key={index}
              style={styles.image}
              source={{uri: 'data:image/png;base64,' + image}}
            />
          ))}
      </ScrollView>
      <View style={styles.messageInputWrapper}>
        <TextInput
          onChangeText={event => handleInput(event)}
          value={messageValue}
          style={styles.messageInput}
          placeholderTextColor={'#bababa'}
          placeholder='Type your message here...'
        />
        <TouchableOpacity
          onPress={() => onUpdateVisible()}
          style={styles.iconWrapper}>
          <FontAwesomeIcon size={24} color={'#007aff'} icon={faPhotoVideo} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleSubmitMessage}
          style={styles.iconWrapper}>
          <FontAwesomeIcon size={24} color={'#007aff'} icon={faPaperPlane} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    position: 'relative',
  },
  loader: {
    position: 'absolute',
    zIndex: 100,
    width: '100%',
    backgroundColor: '#111111',
    opacity: 0.9,
    height: '100%',
    bottom: 0,
  },
  messageInputWrapper: {
    backgroundColor: '#ebebeb',
    flexDirection: 'row',
    padding: 4,
    color: '#a5a5a5',
  },
  messageInput: {
    height: 60,
    width: '76%',
    color: '#484848',
    padding: 12,
    borderRadius: 8,
  },
  iconWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  images: {
    flexGrow: 0,
    flexShrink: 0,
  },
  image: {
    width: 80,
    height: 80,
    borderColor: 'black',
    borderWidth: 1,
    marginHorizontal: 3,
  },
});
