import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Actions} from 'react-native-router-flux';

import {OneSignalApp} from '../index';
import {changeStatus, saveClientID} from '../redux/actions';
import {createDialog, getListOfTopics} from '../src/utils/firebaseRequests';
import {dialogSelector} from './useDialogSelector';

export const useHomeHook = () => {
  const [topic, setTopic] = useState('');
  const [subTopic, setSubTopic] = useState('');
  const [userName, setUserName] = useState('');
  const [listOfTopics, setListOfTopics] = useState([]);
  const [listOfSubTopics, setListOfSubTopics] = useState([]);
  const {status, clientID} = useSelector(dialogSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    getListOfTopics().then(response => {
      setListOfTopics(response);
    });
  }, []);

  useEffect(() => {
    if (status === 'inQueue') {
      Actions.queue();
    }
    if (status === 'activeDialog') {
      Actions.dialog();
    }
    if (status === 'rate') {
      Actions.rate();
    }
  }, [status]);

  useEffect(() => {
    OneSignalApp.addSubscriptionObserver(event => {
      if (event.to.isSubscribed) {
        dispatch(saveClientID(event.to.userId));
      }
    });
    OneSignalApp.getDeviceState().then(snapshot => {
      if (snapshot.userId) {
        dispatch(saveClientID(snapshot.userId));
      }
    });
  }, [dispatch]);

  const handleSubmit = () => {
    if (
      userName !== '' &&
      topic !== '' &&
      subTopic !== '' &&
      topic &&
      subTopic
    ) {
      createDialog(clientID, userName, topic, subTopic);
      dispatch(changeStatus('inQueue'));
    }
  };

  return {
    status,
    topic,
    subTopic,
    userName,
    listOfTopics,
    listOfSubTopics,
    setTopic,
    setSubTopic,
    setUserName,
    setListOfSubTopics,
    handleSubmit,
  };
};
