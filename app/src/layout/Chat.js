import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {RNCamera} from 'react-native-camera';
import {firebase} from '@react-native-firebase/database';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Modal,
} from 'react-native';

import {saveDialog} from '../../redux/actions';
import {Messages, MessageInput} from '../components/common';
import {dialogSelector} from '../../customHooks/useDialogSelector';

const Chat = () => {
  const {clientID, typing} = useSelector(dialogSelector);
  const [modalVisible, setModalVisible] = useState(false);
  const [images, setImages] = useState([]);
  const dispatch = useDispatch();
  const cameraRef = useRef();

  const takePicture = async () => {
    if (cameraRef) {
      const options = {
        quality: 1,
        base64: true,
        maxWidth: 1920,
        maxHeight: 1080,
      };
      cameraRef.current.takePictureAsync(options).then(data => {
        setImages([data.base64]);
        setModalVisible(false);
      });
    }
  };

  useEffect(() => {
    const dbRef = firebase.database().ref('Active');
    dbRef.on('value', snapshot => {
      if (snapshot.val() === null) {
        return;
      }
      const arrayOfKeys = Object.keys(snapshot.val());
      let results = null;
      arrayOfKeys.map(id => {
        if (snapshot.val()[id].clientID === clientID) {
          results = {id, ...snapshot.val()[id]};
        }
      });
      if (results) {
        dispatch(saveDialog(results));
      } else {
        dbRef.off();
      }
    });
    return () => {
      dbRef.off();
    };
  }, [clientID, dispatch]);

  const handleSendMessage = () => {
    setImages([]);
  };

  return (
    <View style={styles.container}>
      {modalVisible ? (
        <Modal
          animationType='slide'
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setModalVisible(!modalVisible);
          }}>
          <View style={styles.modalContainer}>
            <RNCamera
              ref={cameraRef}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.on}
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={({barcodes}) => {}}
            />
            <View style={styles.cameraButton}>
              <TouchableOpacity onPress={takePicture} style={styles.capture}>
                <Text> Сделать фото </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      ) : (
        <>
          <Messages />
          {typing && <Text>Печтает...</Text>}

          <MessageInput
            images={images}
            onSendMessage={handleSendMessage}
            onUpdateVisible={() => setModalVisible(true)}
          />
        </>
      )}
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  container: {
    flex: 1,
    color: '#fdfefc',
    justifyContent: 'space-between',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  cameraButton: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
