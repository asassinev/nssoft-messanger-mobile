import React from 'react';
import {StyleSheet} from 'react-native';
import {Router, Scene} from 'react-native-router-flux';

import {Home, Queue, Dialog, Rate} from './src/pages';

export const App = () => {
  return (
    <Router>
      <Scene headerMode={false} key={'root'} style={styles.container}>
        <Scene
          key='home'
          onExit={() => {}}
          component={Home}
          initial
          title='Home'
        />
        <Scene key='queue' onExit={() => {}} component={Queue} title='Queue' />
        <Scene
          key='dialog'
          onExit={() => {}}
          component={Dialog}
          title='Dialog'
        />
        <Scene key='rate' onExit={() => {}} component={Rate} title='Rate' />
      </Scene>
    </Router>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
});
