export const SAVE_CLIENT_ID = 'SAVE_DIALOG_ID';
export const SAVE_DIALOG = 'SAVE_DIALOG';
export const CHANGE_STATUS = 'CHANGE_STATUS';
export const END_DIALOG = 'END_DIALOG';

// Typing signals
export const ENABLE_SIGNAL = 'ENABLE_SIGNAL';
export const DISABLE_SIGNAL = 'DISABLE_SIGNAL';
