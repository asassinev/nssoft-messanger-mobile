import React, {useEffect, useRef} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import {
  Animated,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

export const Message = ({message, operatorName}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    fadeIn();
  }, []);

  return (
    <Animated.View
      style={[
        {
          opacity: fadeAnim,
        },
      ]}>
      {message && message.writtenBy === 'client' ? (
        <View style={styles.clientMessage}>
          <Text>Вы {moment(message.timestamp).format('LT')}</Text>
          <Text>{message.content}</Text>
          <View>
            {message.images &&
              message.images.map((image, index) => (
                <Image key={index} style={styles.image} source={{uri: image}} />
              ))}
          </View>
        </View>
      ) : (
        <View style={styles.operatorMessage}>
          <Text>
            {operatorName} {moment(message.timestamp).format('LT')}
          </Text>
          <Text>{message.content}</Text>
          <ScrollView horizontal style={styles.imagesWrapper}>
            {message.images &&
              message.images.map((image, index) => (
                <Image key={index} style={styles.image} source={{uri: image}} />
              ))}
          </ScrollView>
        </View>
      )}
    </Animated.View>
  );
};

Message.propTypes = {
  message: PropTypes.object,
  operatorName: PropTypes.string,
};

const styles = StyleSheet.create({
  clientMessage: {
    alignItems: 'flex-end',
    marginBottom: 12,
  },
  operatorMessage: {
    alignItems: 'flex-start',
    marginBottom: 12,
  },
  imagesWrapper: {
    flexDirection: 'row',
  },
  image: {
    width: 150,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
    marginHorizontal: 3,
  },
});
