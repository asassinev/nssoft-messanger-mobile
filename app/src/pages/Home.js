import React from 'react';
import RNPickerSelect from 'react-native-picker-select';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  ActivityIndicator,
} from 'react-native';

import {useHomeHook} from '../../customHooks/useHomeHook';

export const Home = () => {
  const {
    status,
    userName,
    listOfTopics,
    listOfSubTopics,
    setTopic,
    setSubTopic,
    setUserName,
    setListOfSubTopics,
    handleSubmit,
  } = useHomeHook();

  const onSelectTopic = value => {
    setTopic(value);
    listOfTopics.map(item => {
      if (item.value === value) {
        const result = item.listOfSubTopics.map(item => ({
          label: item,
          value: item,
        }));
        setListOfSubTopics(result);
      }
    });
  };

  return (
    <View style={styles.container}>
      {status === undefined || listOfTopics.length === 0 ? (
        <ActivityIndicator />
      ) : (
        <>
          <View style={styles.item}>
            <Text>Введите имя:</Text>
            <TextInput
              placeholder={'Имя'}
              onChangeText={setUserName}
              value={userName}
            />
          </View>
          <View style={styles.item}>
            <Text>Выберите тему обращения:</Text>
            <RNPickerSelect
              onValueChange={value => onSelectTopic(value)}
              pickerProps={{style: styles.pickerWrapper}}
              items={listOfTopics}
              itemKey={'listOfTopics'}
            />
          </View>
          <View style={styles.item}>
            <Text>Выберите подтему:</Text>
            <RNPickerSelect
              onValueChange={value => setSubTopic(value)}
              pickerProps={{style: styles.pickerWrapper}}
              items={listOfSubTopics}
              itemKey={'listOfSubtopics'}
            />
          </View>
          <Button onPress={handleSubmit} title='Войти в чат' />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 40,
  },
  item: {
    marginBottom: 30,
  },
  input: {
    backgroundColor: '#ffffff',
    height: 30,
    margin: 12,
    borderWidth: 1,
  },
  pickerWrapper: {
    height: 40,
    overflow: 'hidden',
  },
});
