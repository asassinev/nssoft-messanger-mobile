import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {AirbnbRating} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import {endDialog} from '../../redux/actions';
import {closeDialog} from '../utils/firebaseRequests';
import {dialogSelector} from '../../customHooks/useDialogSelector';

export const Rate = () => {
  const [rating, setRating] = useState(0);
  const {dialog} = useSelector(dialogSelector);
  const dispatch = useDispatch();

  const handleChangeRating = rating => {
    setRating(rating);
  };

  const onSubmit = () => {
    if (rating === 0) {
      return;
    }
    closeDialog(dialog, rating);
    dispatch(endDialog());
    Actions.home();
  };

  return (
    <View style={styles.rateContainer}>
      <Text>Диалог завершен Оцените общение со специалистом</Text>
      <AirbnbRating
        count={5}
        reviews={['Bad', 'OK', 'Good', 'Very Good', 'Wow']}
        defaultRating={0}
        size={40}
        onFinishRating={handleChangeRating}
      />

      <TouchableOpacity onPress={onSubmit}>
        <Text style={styles.submitButton}>Оценить</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  rateContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    width: 200,
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#007aff',
    padding: 8,
    color: '#ffffff',
    backgroundColor: '#007aff',
    textAlign: 'center',
  },
});
