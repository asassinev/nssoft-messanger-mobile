import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import dialogReducer from './dialogReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['dialog'],
};

export const rootReducer = persistReducer(
  persistConfig,
  combineReducers({
    dialog: dialogReducer,
  }),
);
