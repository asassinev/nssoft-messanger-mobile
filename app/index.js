import React from 'react';
import {AppRegistry} from 'react-native';
import OneSignal from 'react-native-onesignal';
import {store, persistor} from './redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {ONE_SIGNAL_ID} from '@env';

import {App} from './App';
import {name as appName} from '../app.json';
import 'react-native-gesture-handler';

const OneSignalApp = OneSignal;

OneSignalApp.setLogLevel(6, 0);
OneSignalApp.setAppId(ONE_SIGNAL_ID);

OneSignalApp.setNotificationWillShowInForegroundHandler(
  notificationReceivedEvent => {
    let notification = notificationReceivedEvent.getNotification();
    notificationReceivedEvent.complete(notification);
  },
);

export {OneSignalApp};

const WrapperApp = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => WrapperApp);
