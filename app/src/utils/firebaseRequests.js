import {firebase} from '@react-native-firebase/database';

export const getListOfTopics = async () =>
  await firebase
    .database()
    .ref('listOfTopics')
    .once('value')
    .then(response => {
      let results = response.val().map(item => ({
        label: item.topic,
        value: item.topic,
        listOfSubTopics: item.listOfSubtopics,
      }));

      return results;
    });

export const createDialog = (clientID, userName, topic, subTopic) => {
  const dRef = firebase.database().ref('Waiting');
  dRef.push({
    clientID,
    userName,
    topic,
    subTopic,
  });
};

export const addNotification = async clientID => {
  let results = null;
  let dialogID = null;

  const dbRef = firebase.database().ref('Waiting');
  await dbRef.once('value').then(snapshot => {
    if (snapshot.val() === null) {
      return;
    }
    const arrayOfKeys = Object.keys(snapshot.val());
    arrayOfKeys.map(id => {
      if (snapshot.val()[id].clientID === clientID) {
        dialogID = id;
        results = {remindMe: true, ...snapshot.val()[id]};
      }
    });
  });

  const dRef = firebase.database().ref('Waiting/' + dialogID);
  await dRef.update(results);
};

export const closeDialog = (dialog, rating) => {
  firebase
    .database()
    .ref('Active/' + dialog.id)
    .remove();

  delete dialog.id;

  firebase
    .database()
    .ref('Done')
    .push({rating, endAt: new Date().getTime(), ...dialog});
};
